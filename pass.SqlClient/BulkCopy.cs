﻿namespace pass.SqlClient
{
    public class BulkCopy
    {
        public const string CommandText = "SELECT TOP 0 * FROM dbo.Posts;";
        public const string ConnectionString = "Data Source=localhost;Initial Catalog=AdventureWorks2014;Integrated Security=SSPI;Application Name=pass";

        public static void LoadTable(int batchSize)
        {
            double duration = 0;
            int count = 0;
            System.DateTime start;
            var dt = new System.Data.DataTable();
            using (var connection = new System.Data.SqlClient.SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var da = new System.Data.SqlClient.SqlDataAdapter(CommandText, connection))
                {
                    da.Fill(dt);
                }
            }
            using (var sqlBulkCopy = new System.Data.SqlClient.SqlBulkCopy(ConnectionString))
            {
                sqlBulkCopy.DestinationTableName = "dbo.Posts";
                using (var reader = System.Xml.XmlReader.Create(new System.IO.FileStream("C:\\Data\\Posts.xml", System.IO.FileMode.Open)))
                {
                    reader.MoveToContent();
                    while (reader.Read())
                    {
                        if (reader.NodeType == System.Xml.XmlNodeType.Element)
                        {
                            if (reader.Name == "row")
                            {
                                var xe = System.Xml.Linq.XNode.ReadFrom(reader) as System.Xml.Linq.XElement;
                                if (xe != null)
                                {
                                    var dr = dt.NewRow();
                                    dr["Id"] = xe.Attribute("Id").Value;
                                    dr["PostTypeId"] = xe.Attribute("PostTypeId").Value;
                                    dr["ParentId"] = xe.Attribute("ParentId").ToDbNull();
                                    dr["AcceptedAnswerId"] = xe.Attribute("AcceptedAnswerId").ToDbNull();
                                    dr["CreationDate"] = xe.Attribute("CreationDate").Value;
                                    dr["Score"] = xe.Attribute("Score").Value;
                                    dr["ViewCount"] = xe.Attribute("ViewCount").ToDbNull();
                                    dr["Body"] = xe.Attribute("Body").Value;
                                    dr["OwnerUserId"] = xe.Attribute("OwnerUserId").ToDbNull();
                                    dr["LastEditorUserId"] = xe.Attribute("LastEditorUserId").ToDbNull();
                                    dr["LastEditorDisplayName"] = xe.Attribute("LastEditorDisplayName").ToDbNull();
                                    dr["LastEditDate"] = xe.Attribute("LastEditDate").ToDbNull();
                                    dr["LastActivityDate"] = xe.Attribute("LastActivityDate").Value;
                                    dr["CommunityOwnedDate"] = xe.Attribute("CommunityOwnedDate").ToDbNull();
                                    dr["ClosedDate"] = xe.Attribute("ClosedDate").ToDbNull();
                                    dr["Title"] = xe.Attribute("Title").ToDbNull();
                                    dr["Tags"] = xe.Attribute("Tags").ToDbNull();
                                    dr["AnswerCount"] = xe.Attribute("AnswerCount").ToDbNull();
                                    dr["CommentCount"] = xe.Attribute("CommentCount").Value;
                                    dr["FavoriteCount"] = xe.Attribute("FavoriteCount").ToDbNull();
                                    dt.Rows.Add(dr);
                                    if (dt.Rows.Count >= batchSize)
                                    {
                                        count++;
                                        start = System.DateTime.UtcNow;
                                        sqlBulkCopy.WriteToServer(dt);
                                        duration += System.DateTime.UtcNow.Subtract(start).TotalMilliseconds;
                                        dt.Clear();
                                    }
                                }
                            }
                        }
                    }
                }
                if (dt.Rows.Count > 0)
                {
                    count++;
                    start = System.DateTime.UtcNow;
                    sqlBulkCopy.WriteToServer(dt);
                    duration += System.DateTime.UtcNow.Subtract(start).TotalMilliseconds;
                }
                System.Console.WriteLine("{0} batches took {1} total milliseconds.", count, duration);
            }
        }
    }
}
