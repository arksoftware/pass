﻿-- https://archive.org/details/stackexchange

-- DROP TABLE dbo.Posts;
-- TRUNCATE TABLE dbo.Posts;
IF (OBJECT_ID(N'dbo.Posts') IS NULL)
BEGIN
	CREATE TABLE [dbo].[Posts] (
		[Id] BIGINT NOT NULL PRIMARY KEY,
		[PostTypeId] TINYINT NOT NULL,
		[ParentId] BIGINT NULL,
		[AcceptedAnswerId] BIGINT NULL,
		[CreationDate] DATETIME NOT NULL,
		[Score] INT NOT NULL,
		[ViewCount] INT NULL,
		[Body] NVARCHAR(MAX) NOT NULL,
		[OwnerUserId] INT NULL,
		[LastEditorUserId] INT NULL,
		[LastEditorDisplayName] NVARCHAR(64) NULL,
		[LastEditDate] DATETIME NULL,
		[LastActivityDate] DATETIME NOT NULL,
		[CommunityOwnedDate] DATETIME NULL,
		[ClosedDate] DATETIME NULL,
		[Title] NVARCHAR(256) NULL,
		[Tags] NVARCHAR(256) NULL,
		[AnswerCount] INT NULL,
		[CommentCount] INT NOT NULL,
		[FavoriteCount] INT NULL)
		WITH (DATA_COMPRESSION = PAGE);
END; --IF
GO
/*
USE [master]
GO
ALTER DATABASE [AdventureWorks2014] SET RECOVERY FULL WITH NO_WAIT
GO
USE [master]
GO
ALTER DATABASE [AdventureWorks2014] SET RECOVERY SIMPLE WITH NO_WAIT
GO
*/
