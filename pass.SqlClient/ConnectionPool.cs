﻿namespace pass.SqlClient
{
    public class ConnectionPool
    {
        public const string CommandText = "SELECT @sc = COUNT(*) FROM sys.dm_exec_sessions WHERE program_name = N'pass'";
        public const string ConnectionString = "Data Source=localhost;Initial Catalog=AdventureWorks2014;Integrated Security=SSPI;Application Name=pass;Max Pool Size=100";
        public const string OutputString = "Iteration {0,3:N0} found {1,3:N0} sessions.";
        public const int TestSize = 20;

        private System.Data.SqlClient.SqlCommand GetCommand(System.Data.SqlClient.SqlConnection connection)
        {
            var result = connection.CreateCommand();
            result.CommandText = CommandText;
            result.CommandType = System.Data.CommandType.Text;
            result.Parameters.Add(new System.Data.SqlClient.SqlParameter("@sc", System.Data.SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            });
            return result;
        }

        public void Run(string testType, string runType)
        {
            switch (testType)
            {
                case "lazy":
                    if (runType == "serial")
                    {
                        for (var i = 0; i < TestSize; i++)
                        {
                            TestLazy(i);
                        }
                    }
                    else
                    {
                        System.Threading.Tasks.Parallel.For(0, TestSize, i => TestLazy(i));
                    }
                    break;
                case "managed":
                    var connection = new System.Data.SqlClient.SqlConnection(ConnectionString);
                    connection.Open();
                    if (runType == "serial")
                    {
                        for (var i = 0; i < TestSize; i++)
                        {
                            TestManaged(i, connection);
                        }
                    }
                    else
                    {
                        System.Threading.Tasks.Parallel.For(0, TestSize, i => TestManaged(i, connection));
                    }
                    connection.Close();
                    break;
                case "using":
                    if (runType == "serial")
                    {
                        for (var i = 0; i < TestSize; i++)
                        {
                            TestUsing(i);
                        }
                    }
                    else
                    {
                        System.Threading.Tasks.Parallel.For(0, TestSize, i => TestUsing(i));
                    }
                    break;
            }
        }

        private void TestLazy(int iteration)
        {
            var connection = new System.Data.SqlClient.SqlConnection(ConnectionString);
            connection.Open();
            var command = GetCommand(connection);
            command.ExecuteNonQuery();
            var count = (int)command.Parameters[0].Value;
            System.Console.WriteLine(OutputString, iteration, count);
        }

        private void TestManaged(int iteration, System.Data.SqlClient.SqlConnection connection)
        {
            var command = GetCommand(connection);
            command.ExecuteNonQuery();
            var count = (int)command.Parameters[0].Value;
            System.Console.WriteLine(OutputString, iteration, count);
        }

        private void TestUsing(int iteration)
        {
            int count;
            using (var connection = new System.Data.SqlClient.SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = GetCommand(connection))
                {
                    command.ExecuteNonQuery();
                    count = (int)command.Parameters[0].Value;
                }
            }
            System.Console.WriteLine(OutputString, iteration, count);
        }
    }
}
