﻿namespace pass.SqlClient
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var start = System.DateTime.UtcNow;
                switch (args[0])
                {
                    case "pool":
                        var cp = new ConnectionPool();
                        cp.Run(args[1], args[2]);
                        break;
                    case "person":
                        var p = Person.GetPersonCollection(args[1]);
                        System.Console.WriteLine("Loaded {0} Persons.", p.Count);
                        break;
                    case "async":
                        var a = new Async();
                        a.Test(args[1]);
                        break;
                    case "copy":
                        BulkCopy.LoadTable(System.Int32.Parse(args[1]));
                        break;
                }
                var duration = System.DateTime.UtcNow.Subtract(start).TotalMilliseconds;
                System.Console.WriteLine("Total Duration = {0,4:N0}ms", duration);
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine(e.Message);
                var ae = e as System.AggregateException;
                if (ae != null)
                {
                    foreach (var ie in ae.InnerExceptions)
                    {
                        System.Console.WriteLine(ie.Message);
                    }
                }
            }
        }
    }
}
