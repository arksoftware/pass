﻿namespace pass.SqlClient
{
    public class Async
    {
        public const string ConnectionString = "Data Source=localhost;Initial Catalog=AdventureWorks2014;Integrated Security=SSPI;Application Name=pass";

        public void Test(string method)
        {
            switch (method)
            {
                case "simple":
                    TestSimple();
                    break;
                case "cancel":
                    TestCancel();
                    break;
                case "exceptions":
                    TestExceptions();
                    break;
            }
        }

        private static void TestSimple()
        {
            var t = SimpleAsync();
            System.Console.WriteLine("{0:yyyy-MM-dd HH:mm.ss.fff} - Other Stuff", System.DateTime.UtcNow);
            System.Console.WriteLine("{0:yyyy-MM-dd HH:mm.ss.fff} - Sql Query", t.Result);
        }

        private static async System.Threading.Tasks.Task<System.DateTime> SimpleAsync()
        {
            using (var connection = new System.Data.SqlClient.SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();
                using (var command = connection.CreateCommand())
                {
                    command.Parameters.Add(new System.Data.SqlClient.SqlParameter("@value", System.Data.SqlDbType.DateTime)
                    {
                        Direction = System.Data.ParameterDirection.Output
                    });
                    command.CommandText = "SELECT @value = GETUTCDATE();";
                    command.CommandType = System.Data.CommandType.Text;
                    await command.ExecuteNonQueryAsync();
                    return (System.DateTime)command.Parameters[0].Value;
                }
            }
        }

        private static void TestExceptions()
        {
            try
            {
                var q = new System.Collections.Generic.List<string>
                {
                    "SELECT @value = @@VERSION;",
                    "SELECT TOP 1 @value = OBJECT_NAME([object_id]) FROM sys.partitions ORDER BY [rows] DESC;",
                    "WAITFOR DELAY '00:00:10'; SELECT TOP 1 @value = [CarrierTrackingNumber] FROM [Sales].[SalesOrderDetail] ORDER BY NEWID();",
                    "SELECT @value = CAST(2 / 0 AS NVARCHAR(4000));"
                };
                System.Threading.Tasks.Parallel.ForEach(q, RunQuery);
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine(e.Message);
                var ae = e as System.AggregateException;
                if (ae != null)
                {
                    foreach (var ie in ae.InnerExceptions)
                    {
                        System.Console.WriteLine(ie.Message);
                    }
                }
            }
            System.Console.WriteLine("Done");
        }

        private static void RunQuery(string query)
        {
            using (var connection = new System.Data.SqlClient.SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = query;
                    command.CommandType = System.Data.CommandType.Text;
                    command.Parameters.Add(new System.Data.SqlClient.SqlParameter("@value", System.Data.SqlDbType.NVarChar, 4000)
                    {
                        Direction = System.Data.ParameterDirection.Output
                    });
                    command.ExecuteNonQuery();
                    System.Console.WriteLine(command.Parameters[0].Value);
                }
            }
        }

        private static void TestCancel()
        {
            var source = new System.Threading.CancellationTokenSource();
            try
            {
                System.Threading.Tasks.Task.WaitAll(new []
                {
                    RunQueryAsync("WAITFOR DELAY '00:10:00';", source.Token),
                    RunQueryAsync("WAITFOR DELAY '00:10:00';", source.Token),
                    RunQueryAsync("WAITFOR DELAY '00:10:00';", source.Token),
                    RunQueryAsync("WAITFOR DELAY '00:10:00';", source.Token),
                    RunQueryAsync("SELECT 2 / 0;", source.Token)
                });
            }
            catch (System.Exception e)
            {
                //source.Cancel();
                System.Console.WriteLine(e.Message);
                var ae = e as System.AggregateException;
                if (ae != null)
                {
                    foreach (var ie in ae.InnerExceptions)
                    {
                        System.Console.WriteLine(ie.Message);
                    }
                }
            }
            System.Console.WriteLine("Done");
        }

        private static async System.Threading.Tasks.Task RunQueryAsync(string query, System.Threading.CancellationToken token)
        {
            using (var connection = new System.Data.SqlClient.SqlConnection(ConnectionString))
            {
                await connection.OpenAsync(token);
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = query;
                    command.CommandType = System.Data.CommandType.Text;
                    command.Parameters.Add(new System.Data.SqlClient.SqlParameter("@value", System.Data.SqlDbType.NVarChar, 4000)
                    {
                        Direction = System.Data.ParameterDirection.Output
                    });
                    await command.ExecuteNonQueryAsync(token);
                    System.Console.WriteLine(command.Parameters[0].Value);
                }
            }
        }
    }
}
