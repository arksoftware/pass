﻿using System.Linq;

namespace pass.SqlClient
{
    public class Person
    {
        public const string CommandText = "SELECT * FROM Person.Person;";
        public const string ConnectionString = "Data Source=localhost;Initial Catalog=AdventureWorks2014;Integrated Security=SSPI;Application Name=pass";

        public readonly int BusinessEntityID;
        public readonly string PersonType;
        public readonly bool NameStyle;
        public readonly string Title;
        public readonly string FirstName;
        public readonly string MiddleName;
        public readonly string LastName;
        public readonly string Suffix;
        public readonly int EmailPromotion;
        public readonly string AdditionalContactInfo;
        public readonly System.Xml.Linq.XElement Demographics;
        public readonly System.Guid rowguid;
        public readonly System.DateTime ModifiedDate;
        public readonly System.DateTime? BirthDate;

        private Person(object[] values)
        {
            BusinessEntityID = (int)values[0];
            PersonType = (string)values[1];
            NameStyle = (bool)values[2];
            Title = (string)values[3].FromDbNull();
            FirstName = (string)values[4];
            MiddleName = (string)values[5].FromDbNull();
            LastName = (string)values[6];
            Suffix = (string)values[7].FromDbNull();
            EmailPromotion = (int)values[8];
            AdditionalContactInfo = (string)values[9].FromDbNull();
            Demographics = System.Xml.Linq.XElement.Parse((string)values[10]);
            rowguid = (System.Guid)values[11];
            ModifiedDate = (System.DateTime)values[12];

            var xe = Demographics.Descendants("BirthDate").FirstOrDefault();
            BirthDate = xe == null ? null : (System.DateTime?)System.DateTime.Parse(xe.Value);
        }

        public static System.Collections.Generic.Dictionary<int, Person> GetPersonCollection(string method)
        {
            return method == "fast" ? FasterMethod() : NormalMethod();
        }

        private static System.Collections.Generic.Dictionary<int, Person> NormalMethod()
        {
            var result = new System.Collections.Generic.Dictionary<int, Person>();
            using (var connection = new System.Data.SqlClient.SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = CommandText;
                    command.CommandType = System.Data.CommandType.Text;
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var values = new object[13];
                            reader.GetValues(values);
                            var id = (int)values[0];
                            result.Add(id, new Person(values));
                        }
                    }
                }
            }
            return result;
        }

        private static System.Collections.Generic.Dictionary<int, Person> FasterMethod()
        {
            var result = new System.Collections.Generic.Dictionary<int, Person>();
            var buffer = new System.Collections.Generic.List<object[]>();
            using (var connection = new System.Data.SqlClient.SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = CommandText;
                    command.CommandType = System.Data.CommandType.Text;
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var values = new object[13];
                            reader.GetValues(values);
                            buffer.Add(values);
                        }
                    }
                }
            }
            foreach (var i in buffer)
            {
                var id = (int)i[0];
                result.Add(id, new Person(i));
            }
            return result;
        }
    }
}
